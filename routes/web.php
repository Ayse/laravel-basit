<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\middleware\kotuKelime;

Route::get('/', 'App\Http\Controllers\HomeController@index')->name('anasayfa');

Route::get('/home/{slug}', 'App\Http\Controllers\HomeController@uye')->name('anasayfa');

Route::get('/home/{slug}/{id}', 'App\Http\Controllers\HomeController@paylasimlar')->name('anasayfa');

Route::get('/kitap/{slug?}/{yil?}/{yazar?}', 'App\Http\Controllers\deneme@kitap')->name('anasayfa');

Route::get('/yas/{age}', function($age){
    return date('Y')-$age. 'yaşındasınız.';
})->where('age', '[0-9]+');

Route::get('/hakkimizda', 'App\Http\Controllers\deneme@hakkimizda')->name('hakkimizda');

Route::prefix('elektronik')->group(function(){
    Route::get('telefon', function(){
        return 'telefon';
    });
    Route::get('laptop', function(){
        return 'laptop';
    });
});

Route::view('view', 'welcome', ['ad'=>'ayşe']);

Route::get('/iletisim', 'App\Http\Controllers\test@iletisim');

Route::post('/iletisim/post', 'App\Http\Controllers\test@post')-> name('iletisim.post');

Route::get('/kayit', 'App\Http\Controllers\Validation@index');

Route::post('/kayit-ol', 'App\Http\Controllers\Validation@post')->middleware('kotu')->name('kayit.post');

Route::get('/index', 'App\Http\Controllers\Test@index');
Route::get('/iliski', 'App\Http\Controllers\Test@iliski');


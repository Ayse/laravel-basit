<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function uye($uye){
        return $uye;
    }

    public function paylasimlar($uye, $id){
        return $uye. "' e ait " .$id. " numaralı paylaşım";
    }

    public function index(){
        $data['names']=["Ayşe", "Ahmet", "Mehmet"];
        $data['blog']="<b>Kalın yazı</b>rewgtgerergergrgegre";
        return view('anasayfa', $data);
    }
}

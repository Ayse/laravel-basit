<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Validation extends Controller
{
    public function index(){
        return view('kayit');
    }

    public function post(Request $request){
        $request->validate([
            'ad'=>'required | min:3',
            'email'=> 'required|email' ,
            'sifre'=>'required|min:6|confirmed',

        ]);

        echo "kuralları geçti";
    }
}
